#!/bin/env bash
# Use this script to add s3 caching to nix build jobs.
#
# Requirements:
# - an s3 bucket
# - these environment variables:
#     NIX_S3_CACHE, NIX_CACHE_PRIVATE_KEY, NIX_CACHE_PUBLIC_KEY

# check required env vars
: "${NIX_S3_CACHE:? not set or empty, required for nix build}"
: "${NIX_CACHE_PRIVATE_KEY:? not set or empty, required to sign derivations}"
: "${NIX_CACHE_PUBLIC_KEY:? not set or empty, required to trust S3 cache}"
[[ -n "${AWS_ACCESS_KEY_ID}" || -n "${AWS_SHARED_CREDENTIALS_FILE}" ]] || { echo "AWS credentials required for s3 read/write"; exit 1; }

# create a function to run nix build commands with a s3 cache
function with-s3-cache {
  echo Running nix function with s3 substituter: $NIX_S3_CACHE
  $@ \
    --extra-experimental-features nix-command \
    --extra-experimental-features flakes \
    --extra-substituters "${NIX_S3_CACHE}" \
    --extra-trusted-public-keys "${NIX_CACHE_PUBLIC_KEY}" \
    --post-build-hook /tmp/post-build-hook.sh
}

# create a post-build-hook script to sign and upload outputs
cat > /tmp/post-build-hook.sh << 'EOF'
  set -euf
  export IFS=' '
  echo Signing and copying build outputs to ${NIX_S3_CACHE}: ${OUT_PATHS}
  nix store sign --key-file <(echo ${NIX_CACHE_PRIVATE_KEY}) ${OUT_PATHS}
  nix copy --to ${NIX_S3_CACHE} ${OUT_PATHS}
EOF
chmod a+x /tmp/post-build-hook.sh
